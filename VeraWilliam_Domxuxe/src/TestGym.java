import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;


class TestGym {

	@Test
	void test() {
		assertEquals("NOPE", Gym.comprovacioNormal(2, 1, 60));
		assertEquals("NOPE", Gym.comprovacioNormal(3, 1, 30));
		assertEquals("SE NOTA EL GYM", Gym.comprovacioNormal(5, 2, 51));
		assertEquals("NOPE", Gym.comprovacioNormal(1, 2, 20));
		assertEquals("SE NOTA EL GYM", Gym.comprovacioNormal(3, 1, 40));
		
		
	}

	@Test
	void test2() {
		assertEquals("NOPE", Gym.comprovacioNormal(3, 2, 50));
		assertEquals("SE NOTA EL GYM", Gym.comprovacioNormal(5, 1, 2000));
		assertEquals("SE NOTA EL GYM", Gym.comprovacioNormal(5, 2, 5000));
		assertEquals("NOPE", Gym.comprovacioNormal(4, 1, 20));
		assertEquals("ERROR", Gym.comprovacioNormal(-1, 1, 40));
		assertEquals("ERROR", Gym.comprovacioNormal(20, 1, 40));
		
	}
}
